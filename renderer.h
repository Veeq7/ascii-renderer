#ifndef RENDERER_H_
#define RENDERER_H_

typedef struct {
    unsigned char r;
    unsigned char g;
    unsigned char b;
    unsigned char a;
} RGBA;

RGBA rgba(int hex);

#define SCREEN_WIDTH 120
#define SCREEN_HEIGHT 40

typedef struct {
    RGBA color[SCREEN_WIDTH][SCREEN_HEIGHT];
    RGBA background[SCREEN_WIDTH][SCREEN_HEIGHT];
    char chars[SCREEN_WIDTH][SCREEN_HEIGHT];
    void* console;
} Renderer;

void renderer_init(Renderer* renderer);
void renderer_clear(Renderer* renderer);
void renderer_draw(Renderer* renderer);
void renderer_blit_rect(Renderer* renderer, int x, int y, int w, int h, RGBA rgba);
void renderer_blit_circle(Renderer* renderer, int x, int y, int r, RGBA rgba);
void renderer_blit_line(Renderer* renderer, int x0, int y0, int x1, int y1, RGBA rgba);
void renderer_blit_liner(Renderer* renderer, int x0, int y0, int x1, int y1, int r, RGBA rgba);
void renderer_blit_text(Renderer* renderer, int x, int y, const char* text, RGBA rgba);
void renderer_blit_image(Renderer* renderer, const char* filename, int x, int y);

#endif