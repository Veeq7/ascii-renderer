#ifndef PARSER_H_
#define PARSER_H_

typedef struct {
    int argc;
    char** argv;
} ArgParser;

ArgParser parse_args(int argc, char** argv);
char* get_string_arg(ArgParser* parser);
int get_int_arg(ArgParser* parser);
long long get_hex_arg(ArgParser* parser);
float get_float_arg(ArgParser* parser);

#endif