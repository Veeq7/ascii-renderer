#include "parser.h"
#include <stdlib.h>

ArgParser parse_args(int argc, char** argv) {
    ArgParser parser = {0};
    parser.argc = argc;
    parser.argv = argv;
    return parser;
}

char* get_string_arg(ArgParser* parser) {
    if (parser->argc) {
        --parser->argc;
        return *parser->argv++;
    }
    return 0;
}

int get_int_arg(ArgParser* parser) {
    if (parser->argc) {
        --parser->argc;
        return atoi(*parser->argv++);
    }
    return 0;
}

long long get_hex_arg(ArgParser* parser) {
    if (parser->argc) {
        --parser->argc;
        return strtoll(*parser->argv++, 0, 16);
    }
    return 0;
}

float get_float_arg(ArgParser* parser) {
    if (parser->argv) {
        --parser->argc;
        return atof(*parser->argv++);
    }
    return 0;
}