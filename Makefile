CC = gcc
CFLAGS = -O2 -Wall -Werror -pedantic -ggdb
INCLUDE = 
LIBS = 

all: ascre.exe

ascre.exe: main.c renderer.c parser.c
	$(CC) $(CFLAGS) $(INCLUDE) $(LIBS) $^ -o $@

clean:
	del ascre.exe