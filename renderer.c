#include "renderer.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <windows.h>

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

RGBA rgba(int hex) {
    RGBA color;
    color.r = hex >> 8 * 3 & 0xff;
    color.g = hex >> 8 * 2 & 0xff;
    color.b = hex >> 8 * 1 & 0xff;
    color.a = hex >> 8 * 0 & 0xff;
    return color;
}

void renderer_init(Renderer* renderer) {
    renderer->console = GetStdHandle(STD_OUTPUT_HANDLE);
    renderer_clear(renderer);
}

void renderer_clear(Renderer* renderer) {
    for (int x = 0; x < SCREEN_WIDTH; ++x) {
        for (int y = 0; y < SCREEN_HEIGHT; ++y) {
            renderer->background[x][y] = rgba(0x00000000);
            renderer->color[x][y] = rgba(0x00000000);
        }
    }
}

static void renderer_set_color(Renderer* renderer, RGBA color, RGBA background) {
    int attrib = 0;
    {
        if (color.r >= 192 || color.g >= 192 || color.b >= 192) {
            attrib |= FOREGROUND_INTENSITY;
        }
        if (color.r >= 128) {
            attrib |= FOREGROUND_RED;
        }
        if (color.g >= 128) {
            attrib |= FOREGROUND_GREEN;
        }
        if (color.b >= 128) {
            attrib |= FOREGROUND_BLUE;
        }
    }
    {
        if (background.r >= 192 || background.g >= 192 || background.b >= 192) {
            attrib |= BACKGROUND_INTENSITY;
        }
        if (background.r >= 128) {
            attrib |= BACKGROUND_RED;
        }
        if (background.g >= 128) {
            attrib |= BACKGROUND_GREEN;
        }
        if (background.b >= 128) {
            attrib |= BACKGROUND_BLUE;
        }
    }
    SetConsoleTextAttribute(renderer->console, attrib); 
}

static void renderer_print_color(Renderer* renderer, RGBA color, RGBA background, unsigned char c) {
    renderer_set_color(renderer, color, background);

    if (!c)
    {
        if (color.a >= 255) {
            c = 219; // █
        } else if (color.a >= 192) {
            c = 178; // ▓
        } else if (color.a >= 128) {
            c = 177; // ▒
        } else if (color.a >= 64) {
            c = 176; // ░
        } else {
            c = ' ';
        }
    }
    putc(c, stdout);
}

static void renderer_print_separator(Renderer* renderer) {
    renderer_set_color(renderer, rgba(0xffffffff), rgba(0));
    for (int x = 0; x < SCREEN_WIDTH; ++x) {
        putc('-', stdout);
    }
    putc('|', stdout);
    putc('\n', stdout);
}

void renderer_draw(Renderer* renderer) {
    renderer_print_separator(renderer);
    for (int y = 0; y < SCREEN_HEIGHT; ++y) {
        for (int x = 0; x < SCREEN_WIDTH; ++x) {
            renderer_print_color(renderer, renderer->color[x][y], renderer->background[x][y], renderer->chars[x][y]);
        }
        renderer_set_color(renderer, rgba(0xffffffff), rgba(0));
        putc('|', stdout);
        putc('\n', stdout);
    }
    renderer_print_separator(renderer);
    renderer_set_color(renderer, rgba(0xffffffff), rgba(0));
}

static void screen_clamp(int* x, int* y) {
    if (*x < 0) *x = 0;
    if (*x >= SCREEN_WIDTH) *x = SCREEN_WIDTH - 1;
    
    if (*y < 0) *y = 0;
    if (*y >= SCREEN_HEIGHT) *y = SCREEN_HEIGHT - 1;
}

static RGBA blend_colors(RGBA old, RGBA new) {
    // This blending is pretty cool, but it doesn't work well with limited console color palette
    //float new_blend = new.a / 255.0;
    //float old_blend = 1 - new_blend;

    //new.r = old.r * old_blend + new.r * new_blend;
    //new.g = old.g * old_blend + new.g * new_blend;
    //new.b = old.b * old_blend + new.b * new_blend;
    new.a = max(old.a, new.a);

    return new;
}

static void renderer_set_char(Renderer* renderer, int x, int y, RGBA color, char c) {
    if (c) {
        renderer->background[x][y] = blend_colors(renderer->background[x][y], renderer->color[x][y]);
        renderer->color[x][y] = blend_colors(renderer->color[x][y], color);
        renderer->chars[x][y] = c;
    } else {
        renderer->color[x][y] = blend_colors(renderer->background[x][y], renderer->color[x][y]);
        renderer->color[x][y] = blend_colors(renderer->color[x][y], color);
        renderer->background[x][y] = rgba(0);
        renderer->chars[x][y] = 0;
    }
}

void renderer_blit_rect(Renderer* renderer, int x, int y, int w, int h, RGBA color) {
    screen_clamp(&x, &y);
    int x1 = x + w - 1;
    int y1 = y + h - 1;
    screen_clamp(&x1, &y1);

    for (int xx = x; xx <= x1; ++xx) {
        for (int yy = y; yy <= y1; ++yy) {
            renderer_set_char(renderer, xx, yy, color, 0);
        }
    }
}

void renderer_blit_circle(Renderer* renderer, int x, int y, int r, RGBA color) {
    int x0 = x - r;
    int y0 = y - r;
    int x1 = x + r;
    int y1 = y + r;
    screen_clamp(&x0, &y0);
    screen_clamp(&x1, &y1);

    for (int xx = x0; xx <= x1; ++xx) {
        for (int yy = y0; yy <= y1; ++yy) {
            float dist = hypot(xx - x, yy - y);
            if (dist <= r) {
                RGBA local_color = color;
                local_color.a *= min(1, 1.5 - dist / r);
                renderer_set_char(renderer, xx, yy, local_color, 0);
            }
        }
    }
}

void renderer_blit_line(Renderer* renderer, int x0, int y0, int x1, int y1, RGBA color) {
    screen_clamp(&x0, &y0);
    screen_clamp(&x1, &y1);

    float y = y0;
    float y_diff = (float)(y1 - y0) / (x1 - x0);
    for (int xx = 0; xx <= x1; ++xx) {
        y += y_diff;
        int yy = floor(y);
        renderer_set_char(renderer, xx, yy, color, 0);
    }
}

void renderer_blit_liner(Renderer* renderer, int x0, int y0, int x1, int y1, int r, RGBA color) {
    screen_clamp(&x0, &y0);
    screen_clamp(&x1, &y1);

    float y = y0;
    float y_diff = (float)(y1 - y0) / (x1 - x0);
    for (int xx = 0; xx <= x1; ++xx) {
        y += y_diff;
        int yy = floor(y);
        renderer_blit_circle(renderer, xx, yy, r, color);
    }
}

void renderer_blit_text(Renderer* renderer, int x, int y, const char* text, RGBA color) {
    screen_clamp(&x, &y);
    size_t len = strlen(text);
    for (int i = 0; i < len && i < SCREEN_WIDTH; ++i) {
        int xx = x + i;
        renderer_set_char(renderer, xx, y, color, text[i]);
    }
}

void renderer_blit_image(Renderer* renderer, const char* filename, int x, int y) {
    int w, h, c;
    stbi_uc* image = stbi_load(filename, &w, &h, &c, 4);

    screen_clamp(&x, &y);
    int x1 = x + w - 1;
    int y1 = y + h - 1;
    screen_clamp(&x1, &y1);
    for (int xx = x; xx <= x1; ++xx) {
        for (int yy = y; yy <= y1; ++yy) {
            RGBA color = ((RGBA*)image)[xx - x + (yy - y) * w];
            renderer_set_char(renderer, xx, yy, color, 0);
        }
    }

    free(image);
}