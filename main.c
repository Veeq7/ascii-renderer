#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "renderer.h"
#include "parser.h"

static void usage(char* program) {
    printf("Usage: ");
    printf("%s [commands]\n", program);
    printf("Commands: \n");
    printf("\timage <path> <x> <y>\n");
    printf("\trect <x> <y> <w> <h> <hex>\n");
    printf("\tline <x0> <y0> <x1> <y1> <hex>\n");
    printf("\tliner <x0> <y0> <x1> <y1> <r> <hex>\n");
    printf("\tcircle <x> <y> <r> <hex>\n");
    printf("\ttext <x> <y> <text> <hex>\n");
    printf("\ttest\n");
}

int main(int argc, char** argv) {
    ArgParser parser = parse_args(argc, argv);
    char* program = get_string_arg(&parser);
    if (!parser.argc) {
        usage(program);
        return 0;
    }

    Renderer renderer = {0};
    renderer_init(&renderer);
    for (const char* arg = get_string_arg(&parser); arg; arg = get_string_arg(&parser)) {
        if (strcmp(arg, "image") == 0) {
            const char* path = get_string_arg(&parser);
            int x = get_int_arg(&parser);
            int y = get_int_arg(&parser);
            
            renderer_blit_image(&renderer, path, x, y);
        } else if (strcmp(arg, "rect") == 0) {
            int x = get_int_arg(&parser);
            int y = get_int_arg(&parser);
            int w = get_int_arg(&parser);
            int h = get_int_arg(&parser);
            long long hex = get_hex_arg(&parser);
            
            renderer_blit_rect(&renderer, x, y, w, h, rgba(hex));
        } else if (strcmp(arg, "line") == 0) {
            int x0 = get_int_arg(&parser);
            int y0 = get_int_arg(&parser);
            int x1 = get_int_arg(&parser);
            int y1 = get_int_arg(&parser);
            long long hex = get_hex_arg(&parser);
            
            renderer_blit_line(&renderer, x0, y0, x1, y1, rgba(hex));
        } else if (strcmp(arg, "liner") == 0) {
            int x0 = get_int_arg(&parser);
            int y0 = get_int_arg(&parser);
            int x1 = get_int_arg(&parser);
            int y1 = get_int_arg(&parser);
            int r = get_int_arg(&parser);
            long long hex = get_hex_arg(&parser);
            
            renderer_blit_liner(&renderer, x0, y0, x1, y1, r, rgba(hex));
        } else if (strcmp(arg, "circle") == 0) {
            int x = get_int_arg(&parser);
            int y = get_int_arg(&parser);
            int r = get_int_arg(&parser);
            long long hex = get_hex_arg(&parser);

            renderer_blit_circle(&renderer, x, y, r, rgba(hex));
        } else if (strcmp(arg, "text") == 0) {
            int x = get_int_arg(&parser);
            int y = get_int_arg(&parser);
            const char* text = get_string_arg(&parser);
            long long hex = get_hex_arg(&parser);

            renderer_blit_text(&renderer, x, y, text, rgba(hex));
        } else if (strcmp(arg, "test") == 0) {
            renderer_blit_image(&renderer, "pog.png", 30, 5);
            renderer_blit_rect(&renderer, 0, 0, 10, 1, rgba(0xff0000ff));
            renderer_blit_circle(&renderer, 5, 5, 5, rgba(0x808000ff));
            renderer_blit_liner(&renderer, 5, 10, 25, 15, 5, rgba(0x800000ff));
            renderer_blit_line(&renderer, 0, 0, 25, 10, rgba(0x000080ff));
            renderer_blit_text(&renderer, 5, 5, "Hello three!", rgba(0xff0000ff));
        } else {
            printf("Incorrect argument: %s\n", arg);
            usage(argv[0]);
            return 0;
        }
    }
    renderer_draw(&renderer);
    
    return 0;
}